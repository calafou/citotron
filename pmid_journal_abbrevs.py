#!/usr/bin/python3
# Make a JSON dictionary from J_Entrez.txt file downloaded from here:
#   ftp://ftp.ncbi.nih.gov/pubmed/J_Entrez.txt
# * The dictionary only has the abbreviations.
# * Key is the abbreviation and value is the expanded form.
from json import dump
from pprint import pprint
with open("data/J_Entrez.txt", 'rt', encoding='ascii') as f:
    lines = f.readlines()
shortforms = [l.split(':')[1].strip() for l in lines if 'MedAbbr' in l]
longforms = [l.split(':')[1].strip() for l in lines if 'JournalTitle' in l]
abbrevs = {k:v for k,v in zip(shortforms,longforms)}
with open("data/pmid_journal_abbrevs.json", 'w', encoding='utf-8') as f:
    dump(abbrevs, f, indent=0, ensure_ascii=False)
pprint(abbrevs)
print("Output is in data/pmid_journal_abbrevs.json")

